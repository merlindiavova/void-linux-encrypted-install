# Void Linux Encrypted Install

A simple semi-automated script to achieve full disk encryption (FDE) on [Void Linux](https://voidlinux.org/). This script favors the [musl](https://www.musl-libc.org/) libc edition, However this can be changed by modify `VLI_MIRROR` in `.env`.

This installation script is inspired by the [Void Full Disk Encryption Guide](https://docs.voidlinux.org/installation/guides/fde.html), however it differs as it does not use the `LVM`.

## WARNING
### For here be dragons

These installation scripts fit my personal use case. **Please read and inspect all files before copying and/or using the files found here.** Incorrect use including providing invalid settings can completely write-off your machine.

## Getting started

**_Again_; Please read and inspect all files before copying and/or using the files found here.**

### Clone/copy this script to target machine

 - I generally boot from the stock musl live image from [Void Linux Downloads](https://alpha.de.repo.voidlinux.org/live/current/).

#### Setup the wifi or ethernet connection on the target machine.
For Wifi Set you can do the following
```sh
 # cp /etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant-<wireless-device-name>.conf
 # wpa_passphrase <wireless-ssid> >> /etc/wpa_supplicant/wpa_supplicant-<wireless-device-name>.conf
 # ln -s /etc/sv/wpa_supplicant /var/service
 # sv restart /var/service/dhcpcd
```
> `wireless-device-name` can be obtained from running `ip a`

#### Clone this repository

```sh
 $ git clone gitlab.com:merlindiavova/void-linux-encrypted-install.git
 $ cd void-linux-encrypted-install
```

### Provide settings
Copy `.env-example` to `./env` and enter the correct settings for the system setup.

```sh
 $ cp .env-example .env
 $ vi .env
 VLI_BOOT_TYPE=MBR
 VLI_FS_TYPE=xfs
 VLI_DEVICE_NAME=
 VLI_DEVICE_ROOT="${VLI_DEVICE_NAME}2"
 VLI_DEVICE_SWAP="${VLI_DEVICE_NAME}1"

# Only required if boot type is EFI
 VLI_DEVICE_EFI="${VLI_DEVICE_NAME}1"

 VLI_HOSTNAME=attach-436321
 VLI_VOLUME_LABEL=attach436321
 VLI_USERNAME=md

 VLI_LOCALE=en_GB.UTF-8
 VLI_HARDWARE_CLOCK=UTC
 VLI_TIMEZONE=Europe/London
 VLI_KEYMAP=uk

 # Remove musl from the end if you want the glibc version
 VLI_MIRROR=https://alpha.de.repo.voidlinux.org/current/musl
```
> The `.env-example` contains all settings required for the installation

#### Boot Type

This script can create a `MBR` or `EFI` setup. Firstly ensure you have set the correct boot type in the `.env` file.

```sh
 $ vi .env
 ...
 VLI_BOOT_TYPE=<type>
```
You can set `VLI_BOOT_TYPE` to either `MBR` or `EFI`

If you chose `EFI` you can edit `partitions/efi.dat`, or if you chose `MBR` you can edit `partitions/mbr.dat`. Please note both files contain sane defaults.

After the `label` each non blank line represents partition instructions. The partition files are processed by [sfdisk](https://linux.die.net/man/8/sfdisk).
For EFI please see the [Partition type GUIDs](https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs).
For MBR please see the [List of partition IDs](https://en.wikipedia.org/wiki/Partition_type#List_of_partition_IDs).

## Run Setup

**_Once Again_; Please read and inspect all files before copying and/or using the files found here.**

Once all the partitions information and other settings have been set to your liking, you are now ready to install void.

```sh
 # ./install-void
```

You will be prompted for your LUKS passphrase, confirmation to accept repository key, root password and your user password. Apart from that everything is automated.

## Reboot
Once the install has completed you will need to reboot the system and login as the new user. You can login as root but that is not required. Run the final command to `finalize the install`.

```sh
 $ cd /home/vli
 # ./finalize-install
```

`finalize-install` checks if the swap partition was successfully mapped to `/dev/mapper/swap` and create an entry for in `etc/fstab`. This script will also remote the `/home/vli` directory on `exit 0`.

## Post Installation

At this point feel free to follow the [Void Linux Post Installation](https://docs.voidlinux.org/config/post-install.html) instructions or take a look at my [Void Linux System Setup](https://gitlab.com/merlindiavova/void-linux-system-setup)

## System-wide Environment Variables

I use many scripts and they often query the same information about my workstation/system such as workstation type `laptop` or `desktop`, has `wifi` or `lan` or both, etc.
So to simplfy things I query these things once during system install and export them as readonly system variables.

```sh
 $ vi /opt/workstation/profile.sh
 WORKSTATION_TYPE=workstation
 # Generally useless but super rarely I port my scripts to other non Linux OS's
 WORKSTATION_OS_TYPE=linux
 WORKSTATION_OS=void-linux-musl
 WORKSTATION_MODEL='ThinkPad T410'
 WORKSTATION_MODEL_FAMILY='ThinkPad T410'
 # If workstation/system has a battery
 WORKSTATION_BATTERY_PATH=/sys/class/power_supply/BAT0
 # If wired interface is present
 WORKSTATION_WIRED_INTERFACE=enp0s25
 # If wireless interface is present
 WORKSTATION_WIFI_INTERFACE=wlp3s0
 export ...
```

## COPYING

Void Linux Encrypted Install is in the public domain.

To the extent possible under law, the creator of this work has waived all
copyright and related or neighboring rights to this work.

[http://creativecommons.org/publicdomain/zero/1.0/](http://creativecommons.org/publicdomain/zero/1.0/)